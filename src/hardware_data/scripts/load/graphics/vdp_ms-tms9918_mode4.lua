return {
  screen = {
    default = {
      width  = 256,
      height = 192,
      fps    = 29.97,
      origin = {
        x = 0,
        y = 0,
      },
    },
    pal     = {
      width  = 256,
      height = 192,
      fps    = 25,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colors = {
    '6bpp',
    limit       = '5bpp',
    transparent = 'colorKey',
  },
  mesh   = false,
  api    = false,
  shader = {
    false,
    fragment = false,
  },
  physic = false,
  sprite = {
    true,
    colors = '4bpp',
    limits = {
      true,
      screen = 64,
      line   =  8,
    },
    rotate = false,
    scale  = false,
    shear  = false,
    affine = false,
    size   = {
      default = {
        width  =  8,
        height =  8,
      },
      big     = {
        width  = 16,
        height = 16,
      },
    },
  },
  tile   = {
    layers = 1,
    {
      tilesSet       = false,
      colors         = '4bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = false,
      mirror         = false,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 8,
          height = 8,
        },
      },
    },
  },
}
