return {
  screen = {
    default = {
      width  = 320,
      height = 224,
      fps    = 29.97,
      origin = {
        x = 0,
        y = 0,
      },
    },
    pal     = {
      width  = 320,
      height = 240,
      fps    = 25,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colors = {
    '15bpp',
    limit       = 61,
    transparent = 'colorKey',
  },
  mesh   = true,
  api    = false,
  shader = {
    false,
    fragment = false,
  },
  physic = false,
  sprite = {
    true,
    colors = '4bpp',
    limits = {
      true,
      screen = 320,
      line   =  80,
    },
    rotate = true,
    scale  = true,
    shear  = true,
    affine = true,
    size   = {
      small    = {
        width  =  8,
        height =  8,
      },
      default  = {
        width  = 16,
        height = 16,
      },
      big      = {
        width  = 24,
        height = 24,
      },
      huge     = {
        width  = 32,
        height = 32,
      },
    },
  },
  tile   = {
    layers = 2,
    a = {
      colors         = '4bpp',
      specifyColors  = false,
      moveIndividual = 'w',
      independent    = 'w',
      mirror         = 'b',
      rotate         = true,
      scale          = true,
      shear          = true,
      affine         = true,
      size           = {
        default = {
          width  =  64,
          height =  64,
        },
        bigW    = {
          width  = 128,
          height =  32,
        },
      },
    },
    b = {
      colors         = '4bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = false,
      mirror         = 'a',
      rotate         = true,
      scale          = true,
      shear          = true,
      affine         = true,
      size           = {
        default = {
          width  =  64,
          height =  64,
        },
        bigW    = {
          width  = 128,
          height =  32,
        },
      },
    },
    w = {
      colors         = '4bpp',
      specifyColors  = false,
      moveIndividual = 'a',
      independent    = 'a',
      mirror         = false,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 32,
          height = 32,
        },
        bigW    = {
          width  = 64,
          height = 32,
        },
      },
    },
  },
}
