return {
  screen = {
    default = {
      width  = 256,
      height = 224,
      fps    = 29.97,
      origin = {
        x = 0,
        y = 0,
      },
    },
    pal     = {
      width  = 256,
      height = 240,
      fps    = 25,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colors = {
    '15bpp',
    limit       = '8bpp',
    transparent = 'averaging',
  },
  mesh   = false,
  api    = false,
  shader = {
    false,
    fragment = false,
  },
  physic = false,
  sprite = {
    true,
    colors = '4bpp',
    limits = {
      true,
      screen = 128,
      line   =  34,
    },
    rotate = false,
    scale  = false,
    shear  = false,
    affine = false,
    size   = {
      small    = {
        width  =  8,
        height =  8,
      },
      default  = {
        width  = 16,
        height = 16,
      },
      big      = {
        width  = 32,
        height = 32,
      },
      huge     = {
        width  = 64,
        height = 64,
      },
    },
  },
  tile   = {
    layers = 4,
    {
      colors         = '2bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = false,
      mirror         = true,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 32,
          height = 32,
        },
        big     = {
          width  = 64,
          height = 64,
        },
        bigW    = {
          width  = 64,
          height = 32,
        },
        bigH    = {
          width  = 32,
          height = 64,
        },
      },
    },
    {
      colors         = '2bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = false,
      mirror         = true,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 32,
          height = 32,
        },
        big     = {
          width  = 64,
          height = 64,
        },
        bigW    = {
          width  = 64,
          height = 32,
        },
        bigH    = {
          width  = 32,
          height = 64,
        },
      },
    },
    {
      colors         = '2bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = false,
      mirror         = true,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 32,
          height = 32,
        },
        big     = {
          width  = 64,
          height = 64,
        },
        bigW    = {
          width  = 64,
          height = 32,
        },
        bigH    = {
          width  = 32,
          height = 64,
        },
      },
    },
    {
      colors         = '2bpp',
      specifyColors  = false,
      moveIndividual = true,
      independent    = false,
      mirror         = true,
      rotate         = false,
      scale          = false,
      shear          = false,
      affine         = false,
      size           = {
        default = {
          width  = 32,
          height = 32,
        },
        big     = {
          width  = 64,
          height = 64,
        },
        bigW    = {
          width  = 64,
          height = 32,
        },
        bigH    = {
          width  = 32,
          height = 64,
        },
      },
    },
  },
}
