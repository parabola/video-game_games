return {
  screen = {
    default = {
      width  = 256,
      height = 224,
      fps    = 29.97,
      origin = {
        x = 0,
        y = 0,
      },
    },
    pal     = {
      width  = 256,
      height = 240,
      fps    = 25,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colors = {
    '15bpp',
    limit       = '8bpp',
    transparent = 'averaging',
  },
  mesh   = false,
  api    = false,
  shader = {
    false,
    fragment = false,
  },
  physic = false,
  sprite = {
    true,
    colors = '4bpp',
    limits = {
      true,
      screen = 128,
      line   =  34,
    },
    rotate = false,
    scale  = false,
    shear  = false,
    affine = false,
    size   = {
      small    = {
        width  =  8,
        height =  8,
      },
      default  = {
        width  = 16,
        height = 16,
      },
      big      = {
        width  = 32,
        height = 32,
      },
      huge     = {
        width  = 64,
        height = 64,
      },
    },
  },
  tile   = {
    layers = 1,
    plane = {
      colors         = '8bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = true,
      mirror         = false,
      rotate         = true,
      scale          = true,
      shear          = true,
      affine         = true,
      size           = {
        default = {
          width  = 128,
          height = 128,
        },
      },
    },
    a = {
      colors         = '7bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = 'plane',
      mirror         = 'b',
      rotate         = true,
      scale          = true,
      shear          = true,
      affine         = true,
      size           = {
        default = {
          width  = 128,
          height = 128,
        },
      },
    },
    b = {
      colors         = '7bpp',
      specifyColors  = false,
      moveIndividual = false,
      independent    = 'plane',
      mirror         = 'a',
      rotate         = true,
      scale          = true,
      shear          = true,
      affine         = true,
      size           = {
        default = {
          width  = 128,
          height = 128,
        },
      },
    },
  },
}
