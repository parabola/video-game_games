--[[ Collection of sprites and parameters to perform the jump movement ]]--

return {
  love.graphics.newQuad(images.bola.x * 5, images.bola.y * 1, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  love.graphics.newQuad(images.bola.x * 6, images.bola.y * 1, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  love.graphics.newQuad(images.bola.x * 7, images.bola.y * 1, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  love.graphics.newQuad(images.bola.x * 8, images.bola.y * 1, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  love.graphics.newQuad(images.bola.x * 9, images.bola.y * 1, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  love.graphics.newQuad(images.bola.x * 5, images.bola.y * 2, images.bola.x, images.bola.y, images.bola[1]:getDimensions()),
  currentFrame    = 1,
  elapsedTime     = 0,
  fps             = 9,
  height          = -250,
  velocity        = 0,
  ground          = windowProfile.mode.height / 2,
  higher          = 0.15,
  higherMax       = 0.15,
  limitButtonJump = false,
  isJumping       = false,
}
