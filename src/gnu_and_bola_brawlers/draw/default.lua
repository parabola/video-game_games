local draw = {}

--Get current time and update several parameters (time, fps print...)
draw.refresh = function()
  local currentTime = love.timer.getTime()
  if nextTime <= currentTime then
    nextTime = currentTime
    return
  end

  love.timer.sleep(nextTime - currentTime)

  love.graphics.scale(windowProfile.scale.x, windowProfile.scale.y)

  love.graphics.print('FPS: ' .. love.timer.getFPS(), 0, 0)
end

--Draw an object using base image  and character data to position, scale, etc...
draw.object = function(images, character)
  love.graphics.draw(
    images[1],
    images.quad,
    character.position.x,
    character.position.y,
    character.orientation,
    character.scale.x,
    character.scale.y,
    character.origin.x,
    character.origin.y
  )
end

return draw
