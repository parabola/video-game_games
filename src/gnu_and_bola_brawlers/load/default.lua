--[[ Default function called from love.load() and initialize game parameters ]]--

local load = {}

load.initialize = function()
  fps = 30
  upTime = love.timer.getTime()
  nextTime = upTime

  windowProfile = require 'load/window_profile'

  love.graphics.setBackgroundColor(0, 232, 216)
  love.graphics.setDefaultFilter('nearest', 'nearest')

  love.window.setMode(windowProfile.mode.width * windowProfile.scale.x, windowProfile.mode.height * windowProfile.scale.y)
  love.window.setTitle(windowProfile.title)

  controller = require 'load/player_controller_configuration'
  images = require 'load/images'
  character = require 'load/character'

  metaSprite = {
    bola = character.bola.stand,
  }
end

return load
