--[[ Contains parameters about the game characters ]]--

return {
  bola = {
    orientation  = 0,
    acceleration = 0,
    velocity     = 125,
    gravity      = -500,
    position     = {
      x = windowProfile.mode.width  / 2,
      y = windowProfile.mode.height / 2,
    },
    scale        = {
      x = 1,
      y = 1,
    },
    origin       = {
      x = images.bola.x / 2,
      y = images.bola.y / 2,
    },
    stand        = require 'meta_sprites/bola/stand',
    walk         = require 'meta_sprites/bola/walk',
    jump         = require 'meta_sprites/bola/jump',
    fall         = require 'meta_sprites/bola/fall',
    actionLeft   = false,
    actionRight  = false,
    actionUp     = false,
    actionDown   = false,
    actionA      = false,
    actionB      = false,
  },
}
